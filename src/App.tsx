import { createContext, useState } from 'react';
import { NavLink, Outlet } from 'react-router-dom';
import { NavBar } from './core/NavBar.tsx';


interface AppConfig {
  language: 'it' | 'en';
  theme: 'dark' | 'light'
}

export const AppContext = createContext<AppConfig>({ language: 'it', theme: 'dark'})
export const ActionsContext = createContext<any>(null)

function App() {
  const [config, setConfig] = useState<AppConfig>({ language: 'en', theme: 'light'})

  function changeLanguage(newlang: 'it' | 'en') {
    setConfig((prev) => ({...prev, language: newlang}))
  }
  function changeTheme(newtheme: 'dark' | 'light') {
    setConfig((prev) => ({...prev, theme: newtheme}))
  }
  return (
    <AppContext.Provider value={config}>
       <NavBar  />
       <ActionsContext.Provider value={{ changeLanguage, changeTheme }}>
        <Outlet />
      </ActionsContext.Provider>
    </AppContext.Provider>
  )
}

export default App
