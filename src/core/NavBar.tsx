import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { AppContext } from '../App.tsx';
import { useAuthStore } from './store/auth.store.ts';

interface PageRule {
  path: Page;
  label: string;
}
type Page =
  'demo1' |
  'demo2' |
  'demo3' |
  'demo4' |
  'demo5' |
  'demo6' |
  'demo7' |
  'demo8' |
  'demo9' |
  'demo10' |
  'catalog'

const pages: PageRule[] = [
  { path: 'demo1', label: 'Demo 1' },
  { path: 'demo2', label: 'Demo 2' },
  { path: 'demo3', label: 'Demo 3' },
  { path: 'demo4', label: 'Demo 4' },
  { path: 'demo5', label: 'Demo 5' },
  { path: 'demo6', label: 'Demo 6' },
  { path: 'demo7', label: 'Demo 7' },
  { path: 'demo8', label: 'Demo 8' },
  { path: 'demo9', label: 'Demo 9' },
  { path: 'demo10', label: 'Demo 10' },
  { path: 'catalog', label: 'Catalog' },
]
export const NavBar = React.memo(() => {
  const logout = useAuthStore(state => state.logout);
  const displayName = useAuthStore(state => state.displayName)

  console.log('render navbar')
  return (
    <div>
    Current Lang:
      <hr/>

      <div className="flex flex-wrap gap-2 flex-row py-3  border-b border-slate-500" >
        {
          pages.map(page => {
            return (
              <NavLink
                key={page.path}
                to={page.path}
                className="primary">{page.label}</NavLink>
            )
          })
        }
        <button onClick={logout}>Logout</button>
        {displayName}
      </div>
    </div>
  )
})
