import { create } from 'zustand';

interface Auth {
  token: string | null;
  displayName: string;
  login: () => void;
  logout: () => void;
}

export const useAuthStore = create<Auth>((set) => ({
  token: null,
  displayName: '',
  login: () => {
    // axios.get
    // ... prendere il risultato
    const res = { token: '123456', displayName: 'Mario'}
    set(res)
  },
  logout: () => {
    // set(prev => ({ token: null, displayName: ''}))
    set(({ token: null, displayName: ''}))
  }
}))
