import { useResize } from '../../uikit/hooks/useResize.ts';
import { Title } from '../../uikit/layout/Title.tsx';

export default function Demo8() {
  const size = useResize()

  return <div>

    <Title text="Demo 8 - custom reusable hook" />
    {size.w} - {size.h}

  </div>
}

