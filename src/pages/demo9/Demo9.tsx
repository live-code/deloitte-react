import React, { PropsWithChildren, useState } from 'react';

export function Demo9() {
  console.log('root')
  const [counter, setCounter] = useState(0)

  const inc = React.useCallback(() => {
    // setCounter(counter + 1);
    setCounter(s => s + 1);
  }, [])

  return  <div>
    Demo 9
    <button onClick={inc}>Inc {counter} </button>
    <Parent title="HELLO PARENT">
      <Child value={counter} />
      <Child2 increment={inc} />
    </Parent>
  </div>
}

interface ParentProps {
  title: string;
}
export const Parent = React.memo((props: PropsWithChildren<ParentProps>) => {
  console.log(' parent')
  return  <div >
    {props.title}
    ...
    {props.children}
    ...
  </div>
})
Parent.displayName = 'Parent'

// Child.tsx
const Child = React.memo((props: any) => {
  console.log('  child')
  return <div>
    Child 1: value: {props.value}
  </div>
})
Child.displayName = 'Child'

// Child2.tsx
interface Child2Props {
  increment: () => void;
}
const Child2 = React.memo((props: Child2Props) => {
  console.log('  child 2')
  return <div>
    Child 2
    <button onClick={props.increment}>Inc</button>
  </div>
})
Child2.displayName = 'Child2'
