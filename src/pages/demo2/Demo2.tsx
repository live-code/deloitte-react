import { useState } from 'react';
import { ServerError } from '../../uikit/layout/ServerError.tsx';
import { SidePanel } from '../../uikit/layout/SidePanel.tsx';
import { Spinner } from '../../uikit/layout/Spinner.tsx';
import { Title } from '../../uikit/layout/Title.tsx';
import { UsersForm } from './components/UsersForm.tsx';
import { UsersList } from './components/UsersList.tsx';
import { useUsers } from './hooks/useUsers.ts';

export default function Demo2() {
  const {
    state: { users, pending, error}, actions
  } = useUsers();

  const [showPanel, setShowPanel] = useState(false)

  return <div>
    <Title text="Demo 2: crud" />

    <i className="fa fa-plus-circle fa-2x"
       onClick={() => setShowPanel(true)}></i>

    { pending && <Spinner variant="primary" />}
    { error && <ServerError />}

    <SidePanel
      show={showPanel}
      title="Form users"
      onClose={() => setShowPanel(false)}
    >

      <UsersForm
        onSave={(formData) => {
          actions.add(formData)
          setShowPanel(false)
        }}
      />
    </SidePanel>

    <UsersList
      data={users}
      onDelete={actions.deleteUser}
      onEdit={actions.editUser}
    />
  </div>
}
