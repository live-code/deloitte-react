import { User } from '../../../model/user.ts';

interface UsersListProps {
  data: User[];
  onDelete: (id: number) => void;
  onEdit: (user: User) => void;
}

export function UsersList(props: UsersListProps) {
  return <>
    {
      props.data.map(user => {
        return <li key={user.id} className="flex flex-col sm:flex-row sm:items-center justify-between">
          <div>{user.name}</div>

          <div className="flex gap-3">
            <button
              disabled={false}
              className="btn primary"
              onClick={() => props.onDelete(user.id)}
            >Delete</button>
            <button
              disabled={false}
              className="btn accent"
              onClick={() => props.onEdit(user)}
            >Edit</button>

          </div>

        </li>
      })
    }
  </>
}
