import { useState } from 'react';
import { Title } from '../../../uikit/layout/Title.tsx';

export interface FormUser {
  name: string;
}

const initialState: FormUser = {
  name: '',
}

interface UsersFormProps {
  onSave: (formData: FormUser) => void;
}

export function UsersForm(props: UsersFormProps) {
  const [formData, setFormData] = useState(initialState)

  function submitHandler(e: React.FormEvent) {
    e.preventDefault()
    props.onSave(formData)
    setFormData(initialState)
  }

  const isValid = formData.name.length > 0;

  return (

      <form onSubmit={submitHandler}>
        <div>

          <div className="mt-6">
            <Title text="ADD USER" />
          </div>
          <input
            type="text" value={formData.name}
            onChange={e => {
              setFormData({...formData, name: e.currentTarget.value})
            }}
          />
        </div>
        <button
          disabled={!isValid}
          type="submit">ADD</button>
      </form>

  )
}
