import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../../model/user.ts';
import { FormUser } from '../components/UsersForm.tsx';

export function useUsers() {
  const [users, setUsers] = useState<User[]>([])
  const [error, setError] = useState(false)
  const [pending, setPending] = useState(false)

  useEffect(() => {
    setPending(true);
    axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then(res => {
        setUsers(res.data)
      })
      .catch(() => {
        setError(true)
      })
      .finally(() => {
        setPending(false);
      })
  }, [])

  function add(formData: FormUser) {
    setError(false)
    setPending(true);
    axios.post<User>(
      'https://jsonplaceholder.typicode.com/usersx',
      formData
    )
      .then(res => {
        setUsers([ ...users, res.data ])
      })
      .catch(() => {
        setError(true)
      })
      .finally(() => {
        setPending(false);
      })
  }

  function deleteUser(id: number) {
    setError(false)
    setPending(true);
    axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(() => {
        setUsers(
          users.filter(u => u.id !== id)
        )
      })
      .catch(() => {
        setError(true)
      })
      .finally(() => {
        setPending(false);
      })
  }

  function editUser(user: User) {
    console.log('edit', user)
  }

  return {
    state: {
      users,
      pending,
      error,
    },
    actions: {
      add,
      deleteUser,
      editUser,
    }

  }
}
