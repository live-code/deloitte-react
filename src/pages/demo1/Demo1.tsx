import { useState } from 'react';
import { Title } from '../../uikit/layout/Title.tsx'; // hook
import Empty from './components/Empty.tsx';
import Total from './components/Total.tsx';

function Demo1() {
  const [products, setProducts] = useState(0)

  function inc() {
    setProducts(products + 1);
    // setProducts(prev => prev + 1);
  }

  return (
    <>
      <Title text="Demo 1: state" />

      <button onClick={inc}>INCREMENT</button>

      <h1>{products}</h1>
      <div>
        {
          products > 0 ?
            <Total value={products} /> :
            <Empty />
        }
      </div>

    </>
  )
}

export default Demo1



