import axios from 'axios';
import { useEffect } from 'react';
import { useAuthStore } from '../../core/store/auth.store.ts';
import { selectFilteredUsers } from './store/users.selectors.ts';
import { useUsersStore } from './store/users.store.ts';

export function Demo10() {
  const auth = useAuthStore();
  const users = useUsersStore(state => state.users);
  const setUsers = useUsersStore(state => state.setUsers);
  const setFilters = useUsersStore(state => state.setFilter);
  const filters = useUsersStore(state => state.filters);

  const derivedUsers = useUsersStore(selectFilteredUsers)
  const filteredUsers = users.filter(u => u.id >= filters.minId)


  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then(res => {
        setUsers(res.data);
      })
  }, [setUsers])

  function changeHandler(event: React.ChangeEvent<HTMLSelectElement>) {
    setFilters({ minId: event.currentTarget.value})
  }

  return <div>
    Demo 10 {JSON.stringify(filters)}

    <select onChange={changeHandler}>
      <option value={0}>tutti</option>
      <option value={5}>Dal 5 in poi</option>
      <option value={9}>Dal 9 in poi</option>
    </select>



    <h1>Users Filtrati</h1>
    {
      users
        .filter(u => u.id >= filters.minId)
        .map(u => {
        return <li key={u.id}>{u.id} - {u.name}</li>
      })
    }

    <h1>Users Filtrati con stato derivato 1</h1>
    {
      filteredUsers
        .map(u => {
        return <li key={u.id}>{u.id} - {u.name}</li>
      })
    }

    <h1>Users Filtrati con stato derivato 2</h1>
    {
      derivedUsers
        .map(u => {
        return <li key={u.id}>{u.id} - {u.name}</li>
      })
    }
    <h1>Tutti gli users</h1>
    {
      users.map(u => {
        return <li key={u.id}>{u.id} - {u.name}</li>
      })
    }

    <div>Token: {auth.token}</div>
    <div>DisplayName: {auth.displayName}</div>

    <button onClick={auth.login}>Login simulation</button>
  </div>
}
