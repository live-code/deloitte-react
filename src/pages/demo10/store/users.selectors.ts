import { UsersState, useUsersStore } from './users.store.ts';

export const selectFilteredUsers = (state: UsersState) => {
  return state.users.filter(u => u.id >= state.filters.minId)
}
