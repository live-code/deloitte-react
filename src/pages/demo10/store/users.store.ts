import { create } from 'zustand';
import { User } from '../../../model/user.ts';

export interface UsersState {
  users: User[];
  filters: { age: number, minId: number, gender: 'M' | 'F' | 'all'},
  setUsers: (users: User[]) => void;
  setFilter: (filters: any) => void;
}

export const useUsersStore = create<UsersState>((set, get) => ({
  users: [],
  filters: { age: 0, minId: 0, gender: 'all'},
  setUsers: (users: User[]) => {
    set({ users })
  },
  setFilter: (newFilters: any) => {
    set(prev => ({
      filters: { ...prev.filters, ...newFilters }
    }))
  }
}))
