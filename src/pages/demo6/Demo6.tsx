import * as React from 'react';
import { useEffect, useRef } from 'react';
import { Title } from '../../uikit/layout/Title.tsx';

export function Demo6() {
  const inputName = useRef<HTMLInputElement>(null)
  const inputSurname = useRef<HTMLInputElement>(null)

  useEffect(() => {
    inputName.current?.focus();
  }, [])

  function save(e: React.FormEvent) {
    e.preventDefault();
    const name = inputName.current?.value;
    const surname = inputSurname.current?.value
    if (name && surname) {
      console.log('...>', name, surname)
    }
  }

  function resetForm() {
    if (inputName.current && inputSurname.current) {
      inputName.current.value = ''
      inputSurname.current.value = ''
    }
  }

  return <div>
    <Title text="Demo6: uncontrolled form" />

    <form onSubmit={save}>
      <input type="text" ref={inputName} placeholder="name" name="name" />
      <input type="text" ref={inputSurname} placeholder="surname" name="surname"/>
      <button type="submit" >SAVE</button>
      <button type="button" onClick={resetForm}>Reset</button>
    </form>
  </div>
}
