import { Card } from '../../uikit/layout/Card.tsx';
import { Title } from '../../uikit/layout/Title.tsx';

export function Demo3() {
  function openUrl() {
    window.open('http://www.google.com')
  }
  function alert() {
    window.alert('ciao')
  }
  return <div>

    <Title text="Demo3: Card" />

    <Card
      title="Card 1"
      icon="bluetooth"
      onIconClick={alert}
    >
      <input type="text"/>
      <input type="text"/>
      <input type="text"/>
    </Card>

    <Card
      variant="accent"
      title="Card 2"
      icon="facebook"
      onIconClick={openUrl}
    >
     bla bla
    </Card>


  </div>
}
