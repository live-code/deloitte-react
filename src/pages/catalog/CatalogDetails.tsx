import { useLoaderData } from 'react-router-dom';
import { Post } from '../../model/post.ts';
import { Title } from '../../uikit/layout/Title.tsx';

export function CatalogDetails() {
  const post = useLoaderData() as Post

  return <div>
    <Title text={post.title} />
    <pre>{post.body}</pre>
  </div>
}
