import { useContext } from 'react';
import { NavLink, useLoaderData } from 'react-router-dom';
import { ActionsContext } from '../../App.tsx';
import { Post } from '../../model/post.ts';

export function Catalog() {
  const posts = useLoaderData() as Post[];
  const actions = useContext(ActionsContext)

  return <div>
    <button onClick={() => actions.changeLanguage('it')}>IT</button>
    <button onClick={() => actions.changeLanguage('en')}>EN</button>
    <button onClick={() => actions.changeTheme('light')}>light</button>
    <button onClick={() => actions.changeTheme('dark')}>dark</button>
    {
      posts?.map(p => {
        return <li key={p.id}>
          <NavLink to={`/catalog/${p.id}`}>
            {p.title}
          </NavLink>
        </li>
      })
    }
  </div>
}
