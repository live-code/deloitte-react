import clsx from 'clsx';
import React, { useState } from 'react';
import { Title } from '../../uikit/layout/Title.tsx';

const initialState = { name: '', surname: ''};

export function Demo7() {
  const [formData, setFormData] = useState(initialState)
  const [dirty, setDirty] = useState(false)

  function submitHandler(e: React.FormEvent) {
    e.preventDefault()
    console.log(formData)
    // POST
    resetHandler()
  }

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const value = e.currentTarget.value;
    const key = e.currentTarget.name;
    setFormData({ ...formData, [key]: value })
    setDirty(true)
  }

  function resetHandler() {
    setDirty(false)
    setFormData(initialState)
  }

  const isNameValid = formData.name.length > 0
  const isSurnameValid = formData.surname.length > 0
  const isValid = isNameValid && isSurnameValid

  return <div>
    <Title text="Demo7"/>

    <form onSubmit={submitHandler}>
      <div>
        {(!isNameValid && dirty) && <div>Campo Required</div>}
        <input
          type="text"
          name="name"
          value={formData.name}
          onChange={onChangeHandler}
          placeholder="Your Name"
          className={clsx(
            {'error': !isNameValid && dirty},
            {'success': isValid},
          )}
        />
      </div>

      <div>
        {(!isSurnameValid && dirty) && <div>Campo Required</div>}
        <input
          type="text"
          name="surname"
          value={formData.surname}
          onChange={onChangeHandler}
          placeholder="Your Surname"
          className={clsx(
            {'error': !isSurnameValid && dirty},
            {'success': isValid},
          )}
        />
      </div>

      <button onClick={resetHandler} type="button">RESET</button>
      <button type="submit" disabled={!isValid}>SAVE</button>
    </form>
  </div>
}
