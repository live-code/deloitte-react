import * as React from 'react';
import { Title } from '../../uikit/layout/Title.tsx';

export function Demo5() {

  function openUrl(url: string, e: React.MouseEvent) {
    console.log(' do something', url, e)
  }

  function doSOmething(e: React.MouseEvent<HTMLButtonElement>) {
    console.log('do somethingg', e.currentTarget )
  }
  function changeHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      window.open(e.currentTarget.value)
      e.currentTarget.value = ''
    }
  }

  return <div>
    <Title text="Demo5" />


    <input type="text" onKeyUp={changeHandler}/>

    <button onClick={doSOmething}>Do something 1</button>
    <button onClick={doSOmething}>Do something 2</button>
    <button onClick={(e) => openUrl('http://fabiobiondi.dev', e)}>fb.dev</button>
    <button onClick={(e) => openUrl('http://google.dev', e)}>google</button>
  </div>
}
