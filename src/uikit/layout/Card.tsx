import clsx from 'clsx';
import { PropsWithChildren } from 'react';
import { Variants } from '../../model/variant.ts';

interface CardProps {
  title: string;
  icon: string;
  onIconClick: () => void;
  variant?: Variants;
}
export function Card(props: PropsWithChildren<CardProps>) {
  const { variant = 'primary' } = props;

  return <div>
    <div
      className={clsx(
        ' p-3 flex items-center justify-between',
        { 'bg-white text-black': variant === 'primary'},
        { 'bg-pink-600 text-white': variant === 'accent'},
      )}
    >
      {props.title}
      <i
        className={`fa fa-${props.icon}`}
        onClick={props.onIconClick}
      />

    </div>
    <div className="border border-white p-3">
      {props.children}
    </div>
  </div>
}
