import css from './Title.module.css';

export interface TitleProps {
  text: string;
}

export function Title(props: TitleProps) {
  return <h1 className={css.title}>{props.text}</h1>
}
