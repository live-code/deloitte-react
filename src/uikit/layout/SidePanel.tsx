import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface SidePanelProps {
  show: boolean;
  title: string
  onClose: () => void;
}

export function SidePanel(props: PropsWithChildren<SidePanelProps>) {
  return  <div className={clsx(
    'fixed w-96 bg-slate-600 top-0 h-full p-4 transition-all duration-700',
    {
      'right-0': props.show,
      '-right-96': !props.show
    }
  )}>
    <i
      onClick={props.onClose}
      className="absolute right-3 top-3 fa fa-times fa-2x"></i>


    <h1>{props.title}</h1>
    {props.children}
  </div>
}
