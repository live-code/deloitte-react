import { useEffect, useState } from 'react';

export function useResize() {
  const [size, setSize] = useState({ w: 0,  h: 0})

  useEffect(() => {
    let timer: number;
    const fn = () => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        setSize({
          w: window.innerWidth,
          h: window.innerHeight
        })
      }, 1000)
    };

    window.addEventListener('resize', fn)

    return () => {
      window.removeEventListener('resize', fn)
    }
  }, [])

  return size;
}
