import { PropsWithChildren } from 'react';

export function PrivateRoute(props: PropsWithChildren) {

  return localStorage.getItem('token') ?
    props.children :
    <div>Pagina riservata. Missing token in in localstorage</div>
}
