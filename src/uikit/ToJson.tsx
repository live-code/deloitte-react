
interface ToJsonProps {
  value: any;
}
/*
export function ToJson(props: ToJsonProps) {
  return <pre>
    {JSON.stringify(props.value, null, 2)}
  </pre>
}
*/

export const ToJson = (props: ToJsonProps) => (
  <pre>{JSON.stringify(props.value, null, 2)}</pre>
)
