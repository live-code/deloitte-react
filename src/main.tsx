import axios from 'axios';
import { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  createRoutesFromElements,
  Navigate,
  NavLink,
  Route,
  RouterProvider
} from 'react-router-dom';
import App from './App.tsx'
import './index.css'
import 'font-awesome/css/font-awesome.min.css'
import { Catalog } from './pages/catalog/Catalog.tsx';
import { CatalogDetails } from './pages/catalog/CatalogDetails.tsx';
import Demo1 from './pages/demo1/Demo1.tsx';
import { Demo10 } from './pages/demo10/Demo10.tsx';
import { Demo3 } from './pages/demo3/Demo3.tsx';
import { Demo4 } from './pages/demo4/Demo4.tsx';
import { Demo5 } from './pages/demo5/Demo5.tsx';
import { Demo6 } from './pages/demo6/Demo6.tsx';
import { Demo7 } from './pages/demo7/Demo7.tsx';
import { Demo9 } from './pages/demo9/Demo9.tsx';
import { PrivateRoute } from './uikit/auth/PrivateRoute.tsx';
import { Spinner } from './uikit/layout/Spinner.tsx';


const Demo2  = lazy(() => import('./pages/demo2/Demo2'))
const Demo8  = lazy(() => import('./pages/demo8/Demo8'))

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<App />}>
        <Route path="demo1" element={<Demo1 />} />
        <Route path="demo2" element={
          <Suspense fallback={<Spinner />}>
          <Demo2 />
          </Suspense>
        } />
        <Route path="demo3" element={<Demo3 />} />
        <Route path="demo4" element={<Demo4 />} />
        <Route path="demo5" element={<Demo5 />} />
        <Route path="demo6" element={<Demo6 />} />
        <Route path="demo7" element={<PrivateRoute><Demo7 /></PrivateRoute>} />
        <Route path="demo8" element={
          <Suspense fallback={<Spinner />}>
            <Demo8 />
          </Suspense>
          }
        />
        <Route path="demo9" element={<Demo9 />} />
        <Route path="demo10" element={<Demo10 />} />
        <Route
          path="catalog"
          element={<Catalog />}
          loader={ async () => {
            const res = await axios.get('https://jsonplaceholder.typicode.com/posts')
            return res.data
          }}
        />

        <Route
          path="catalog/:id"
          element={<CatalogDetails />}
          loader={ async ({params}) => {
            const res = await axios.get('https://jsonplaceholder.typicode.com/posts/' + params.id)
            return res.data
          }}
        />
        <Route path="/" element={
          <Navigate to="demo1" />
        } />

        <Route path="*" element={
          <>
            <div> questa pagina non esiste</div>
            <NavLink to="demo1">go to demos</NavLink>
          </>
        } />
      </Route>


    </>
  )
);

ReactDOM.createRoot(document.getElementById('root')!).render(
    /*<App />*/
  <RouterProvider router={router} fallbackElement={<Spinner />} />
)
